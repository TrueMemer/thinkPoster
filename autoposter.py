# -*- coding: utf-8 -*-

import time
import eventlet
import requests
import logging
import random
import math
import json
import os
import configparser
from time import sleep

from ctypes import *

class Error(Structure):
    _fields_ = [
        ("code", c_int),
        ("msg", c_wchar_p)
    ]

class Group(Structure):
    _fields_ = [
        ("id", c_wchar_p),
        ("name", c_wchar_p),
        ("domain", c_wchar_p),
        ("photo_50", c_wchar_p)
    ]

POST_URL_SNIPPET = 'https://api.vk.com/method/wall.get?domain={}&count={}&filter={}'
POST_USER_URL_SNIPPET = 'https://api.vk.com/method/users.get?fields=photo_50,city,verified&user_ids={}'
POST_GROUP_URL_SNIPPET = "https://api.vk.com/method/groups.getById?&group_id={}"
WALL_POST_URL_SNIPPET = 'https://vk.com/wall-{}_{}'
VIDEO_URL_SNIPPET = "https://vk.com/video-{}_{}"

BOT_TOKEN = None
CHANNEL_NAME = None

DM_CHANNEL_ID = None
RECIPIENT_ID = None

GROUP_NAME = None
GROUP_ID = None
GROUP_ICON = None
GROUP_DOMAIN = None

ACCESS_TOKEN = None

SLEEP_INTERVAL = 120
LOG_FILE = "bot_log.log"
SINGLE_RUN = False

Config = configparser.ConfigParser(allow_no_value=False)

def get_wall(domain = "ittpicrandomm", count = 10, filter_ = "owner"):
    url = POST_URL_SNIPPET.format(domain, count, filter_)
    r = requests.get(url)
    return r

def get_users(user_ids, fields = "photo_50,city,verified"):
    r = requests.get(POST_USER_URL_SNIPPET.format(user_ids)).json()
    return r

def get_group_byid(id):
    r = requests.get(POST_GROUP_URL_SNIPPET.format(id)).json()

    try:
        tmp = r["error"]
        return Error(r["error"]["error_code"], r["error"]["error_msg"])
    except KeyError:
        rr = r["response"][0]
        return Group(rr["gid"], rr["name"], rr["screen_name"], rr["photo"])

    return None

def post_create_message(message):
    headers = {
        "Authorization": "Bot {}".format(BOT_TOKEN),
        "Content-Type": "application/json; charset=utf-8"
    }
    if DM_MODE:
        r = requests.post("https://discordapp.com/api/channels/{}/messages".format(DM_CHANNEL_ID), headers=headers, data=message)
    else:
        r = requests.post("https://discordapp.com/api/channels/{}/messages".format(CHANNEL_NAME), headers=headers, data=message)
    return r

def posts_get():
    timeout = eventlet.Timeout(10)
    try:
        feed = get_wall(GROUP_DOMAIN)
        return feed.json()
    except eventlet.timeout.Timeout:
        logging.warning("[VK] Словили таймаут! Ждем...")
        return None
    finally:
        timeout.cancel()

def posts_send(items, last_id):
    for item in items:
        if item["id"] <= last_id:
            break
        link = WALL_POST_URL_SNIPPET.format(GROUP_ID, item["id"])
        send_discord_message(item)
        time.sleep(1)
    return

def send_discord_message(item):

    logging.info("[Discord] Отправляем сообщение...")

    content = WALL_POST_URL_SNIPPET.format(GROUP_ID, item["id"])

    da = {}

    da["content"] = content
    da["embed"] = {}

    da["embed"]["description"] = item["text"].replace("<br>", "\n") + "\n"

    da["embed"]["image"] = None

    docks = 0

    for attachment in item["attachments"]:
        if attachment["type"] == "video":
            da["embed"]["image"] = {
                "url": attachment["video"]["image_big"]
            }
            da["embed"]["description"] += "\n:film_frames: [{}]({})".format(attachment["video"]["title"], VIDEO_URL_SNIPPET.format(abs(int(attachment["video"]["owner_id"])), attachment["video"]["vid"]))
        if attachment["type"] == "audio":
            da["embed"]["description"] += ":musical_note: [**{} — {}**]({}) [{} sec.]\n".format(attachment["audio"]["artist"], attachment["audio"]["title"], "https://datmusic.xyz/?q={}%20-%20{}".format(attachment["audio"]["artist"], attachment["audio"]["title"]).replace(" ", "%20"), attachment["audio"]["duration"])
        if attachment["type"] == "photo":
            if da["embed"]["image"] is None:
                da["embed"]["image"] = {
                    "url": attachment["photo"]["src_big"]
                }
            else: break
        if attachment["type"] == "doc":
            if docks == 0: da["embed"]["description"] += "\n"
            da["embed"]["description"] += ":page_facing_up: **[{}]({})**\n".format(attachment["doc"]["title"], attachment["doc"]["url"])
            docks+=1
        if attachment["type"] == "link":
            da["embed"]["description"] += ":link: **[{}]({})**\n".format(attachment["link"]["title"], attachment["link"]["url"])

    da["embed"]["color"] = math.floor(random.random() * 0xFFFFFF)

    signed_id_provided = item.get("signer_id")

    if signed_id_provided is not None:
        signer = get_users(signed_id_provided)

        da["embed"]["author"] = {}

        da["embed"]["author"] = {
            "name": "{} {}".format(signer["response"][0]["first_name"], signer["response"][0]["last_name"]),
            "icon_url": signer["response"][0]["photo_50"],
            "url": "https://vk.com/id{}".format(signer["response"][0]["uid"])
        }

    da["embed"]["footer"] = {
        "text": GROUP_NAME,
        "icon_url": GROUP_ICON
    }

    request = post_create_message(json.dumps(da, ensure_ascii=True))
    logging.info(request.text)
    if request.status_code is 200:
        logging.info("[Discord] Отправлено сообщение!")
        return
    elif request.status_code is 401:
        logging.warning("[Discord] Ошибка авторизации!")
        logging.error(request.text)
        return
    else:
        logging.warning("[Discord] Во время оправления сообщения произошла ошибка ({})!".format(request.status_code))
        logging.error(request.text)
        return

def posts_check():
    logging.info("[VK] Начинаем сканирование новых постов...")
    last_id = None
    try:
        last_id = int(Config["APP"]["last_id"])
    except ValueError:
        pass
    if last_id is None:
        logging.error("[VK] Произошла ошибка при чтении из файла!")
        return
    logging.info("[VK] Последний ID = {}".format(last_id))
    
    try:
        feed = posts_get()

        if feed is not None:
            entries = feed["response"][1:]
            
            try:
                tmp = entries[0]["is_pinned"]

                posts_send(entries[1:], last_id)
            except KeyError:
                posts_send(entries, last_id)

            try:
                tmp = entries[0]["is_pinned"]
                Config["APP"]["last_id"] = str(entries[1]["id"])
                with open('autoposter.ini', 'w') as configfile:
                    Config.write(configfile)
                logging.info("[VK] Новый последний пост = {}".format(last_id))
            except KeyError:
                Config["APP"]["last_id"] = str(entries[0]["id"])
                with open('autoposter.ini', 'w') as configfile:
                    Config.write(configfile)
                logging.info("[VK] Новый последний пост = {}".format(last_id))
    except Exception as ex:
        logging.error('Exception of type {!s} in posts_check(): {!s}'.format(type(ex).__name__, str(ex)))
        pass
    
    logging.info("[VK] Сканирование закончено!")
    return

if __name__ == '__main__':
    
    if not os.path.exists("autoposter.ini"):
        logging.error("[APP] Не обнаружен файл 'autoposter.ini'!")
        exit(1)

    try:
        Config.read('autoposter.ini')
    except configparser.ParsingError:
        logging.error("[APP] Возникли ошибки при парсинге конфиг файла. Проверьте синтаксис!")
        exit(1)

    try:
        SINGLE_RUN = Config.getboolean('APP', 'single_mode')
        DM_MODE = Config.getboolean("APP", "dm_mode")
        SLEEP_INTERVAL = Config.getint('APP', 'sleep_interval')
        LOG_FILE = Config.get('APP', 'log_file')
        GROUP_ID = Config.get('APP', 'group_id')

        BOT_TOKEN = Config.get('DISCORD', 'bot_token')
        CHANNEL_NAME = Config.get('DISCORD', 'channel_id')
    except configparser.NoOptionError:
        logging.error("[APP] Все поля в конфигурационном файле обязательны!")
        exit(1)

    logging.basicConfig(format='[%(asctime)s] %(filename)s:%(lineno)d %(levelname)s - %(message)s', level=logging.INFO,
                filename=LOG_FILE, datefmt='%d.%m.%Y %H:%M:%S')

    try:
        GROUP_NAME = Config.get('VK', 'group_name')
        GROUP_DOMAIN = Config.get('VK', 'group_domain')
        GROUP_ICON = Config.get('VK', 'group_icon')
    except configparser.NoSectionError:
        Config.add_section("VK")
        pass
    except configparser.NoOptionError:
        logging.warn("[APP] Не найдено информации о группе. Получаем...")

        g = get_group_byid(GROUP_ID)

        GROUP_NAME = g.name
        GROUP_DOMAIN = g.domain
        GROUP_ICON = g.photo_50

        Config.set("VK", "group_name", GROUP_NAME)
        Config.set("VK", "group_domain", GROUP_DOMAIN)
        Config.set("VK", "group_icon", GROUP_ICON)

    if DM_MODE is True:
        try:
            DM_CHANNEL_ID = Config.get("DISCORD", "dm_channel_id")
        except configparser.NoOptionError:
            try:
                RECIPIENT_ID = Config.get("DISCORD", "recipient_id")
            except configparser.NoOptionError:
                logging.error("[APP] Для работы с личными сообщениями требуется ваш ID! (recipient_id)")
                exit(1)
            headers = {
                "Authorization": "Bot {}".format(BOT_TOKEN),
                "Content-Type": "application/json; charset=utf-8"
            }
            r = requests.get("https://discordapp.com/api/users/@me/channels", headers=headers).json()

            for u in r:
                if u["recipient"]["id"] == RECIPIENT_ID:
                    DM_CHANNEL_ID = u["id"]
                    break
            
            if DM_CHANNEL_ID == None:
                logging.error("[APP] Не можем найти айди канала! Попробуйте написать сообщение боту.")
                exit(1)

    with open('autoposter.ini', 'w') as configfile:
        Config.write(configfile)

    logging.info("[APP] Успешно получили данные группы: ['{}', '{}', '{}']".format(GROUP_NAME, GROUP_DOMAIN, GROUP_ICON))

    #logging.getLogger('requests').setLevel(logging.CRITICAL)
    if not SINGLE_RUN:
        while True:
            posts_check()
            logging.info('[App] Script went to sleep.')
            time.sleep(120)
    else:
        posts_check()
    logging.info('[App] Script exited.\n')